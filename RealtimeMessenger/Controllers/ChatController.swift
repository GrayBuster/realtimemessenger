//
//  ChatController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/30/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

protocol PassingChannelDelegate {
    func passing(_ channel: Channel)
}

class ChatController: UIViewController,UITextFieldDelegate,HandleDismissDelegate,AddingUserDelegate {
    
    func didAdd(_ user: User) {
        if let channel = channel {
            switch channel.uids.count {
            case 0..<2:
                channel.uids.append(user.uid)
                uids.append(user.uid)
                let ref = Database.database().reference().child("Channel")
                let channelRef = ref.childByAutoId()
                channel.id = channelRef.key
                channelRef.updateChildValues(channel.toAny())
            default:
                channel.uids.append(user.uid)
                uids.append(user.uid)
                let ref = Database.database().reference().child("Channel").child(channel.id)
                let inboxController = self.storyboard?.instantiateViewController(withIdentifier: "Inbox Controller") as! InboxController
                inboxController.channel = self.channel
                ref.updateChildValues(channel.toAny())
            }
        }
    }
    
    
    
    var channel: Channel?
    
    var imagePicker = UIImagePickerController()
    
    func handleDismissKeyboard() {
        composeTextField.resignFirstResponder()
    }
    
    
    var uids = [String]()
    
    var receiver: User?

    @IBOutlet weak var sendBtn: UIButton! {
        didSet {
            sendBtn.layer.cornerRadius = 25
        }
    }
    
    var isKeyboardPresented = false
    
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.addCircle(withBorderWidth: 1, .black)
        }
    }
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var containerViewBottomAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var composeTextField: UITextField!
    
    
    @IBOutlet weak var composeView: UIView! {
        didSet {
            composeView.addTopBorder(height: 2, color: .lightGray)
        }
    }
   
    @IBAction func handleCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handleSendImage(_ sender: UIButton) {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !textField.text!.isEmpty {
            handleSendMessage()
            return true
        }
        return false
    }
    
    private func handleSendMessage() {
        guard let message = composeTextField.text, let receiver = receiver else { print("receiver isnt not set yet nil");return }
        if !message.isEmpty {
            if let channel = channel {
                let channelRef = Database.database().reference().child("Channel").child(channel.id)
                let fromId = Auth.auth().currentUser!.uid
                let timestamp = Date().timeIntervalSince1970
                let message = Message(to: receiver.uid, from:  fromId, text: message, time: timestamp, isSeen: false)
                channel.messages.append(message.toAny())
                channelRef.updateChildValues(channel.toAny())
            } else {
                let ref = Database.database().reference().child("Channel")
                let channelRef = ref.childByAutoId()
                let fromId = Auth.auth().currentUser!.uid
                let timestamp = Date().timeIntervalSince1970
                let message = Message(to: receiver.uid, from:  fromId, text: message, time: timestamp, isSeen: false)
                self.channel = Channel(id: channelRef.key, uids: [message.fromId,message.toId], messages: [message.toAny()])
                channelRef.updateChildValues(channel!.toAny())
            
            }
            composeTextField.text = nil
            composeTextField.resignFirstResponder()
        }
    }
    
    @IBAction func handleSend() {
        handleSendMessage()
    }
    
    @objc func handleDismisKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardObserves()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDismisKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if channel != nil {
            if let receiver = receiver {
                if channel!.uids.count > 2 {
                    usernameLabel.text = channel!.name
                } else {
                    usernameLabel.text = receiver.username
                }
                if receiver.image != "" {
                    profileImageView.downloaded(from: receiver.image, completion: nil)
                } else {
                    profileImageView.image = #imageLiteral(resourceName: "user profile")
                }
            }
            
        }else {
            if let receiver = receiver {
                usernameLabel.text = receiver.username
                if receiver.image != "" {
                    profileImageView.downloaded(from: receiver.image, completion: nil)
                } else {
                    profileImageView.image = #imageLiteral(resourceName: "user profile")
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Handle Keyboard Show and Hide
    func setupKeyboardObserves() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        if !isKeyboardPresented {
            let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
            let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
            //containerViewBottomAnchor.constant = -keyboardFrame.height
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardFrame.height
            }
            UIView.animate(withDuration: keyboardDuration) {
                self.view.layoutIfNeeded()
            }
            isKeyboardPresented = true
        }
        
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification) {
        if isKeyboardPresented {
            let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
            let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
            //containerViewBottomAnchor.constant = 0
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardFrame.height
            }
            UIView.animate(withDuration: keyboardDuration) {
                self.view.layoutIfNeeded()
            }
            isKeyboardPresented = false
        }else {
            
        }
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Inbox" {
            if let inboxController = segue.destination as? InboxController {
                inboxController.receiver = receiver
                inboxController.containerView = self.composeView
                inboxController.delegate = self
                inboxController.channel = self.channel != nil ? self.channel : nil
            }
        } else if segue.identifier == "Adding User" {
            if let usersController = segue.destination.content as? UsersTableController {
                usersController.channel = channel
                usersController.receiver = receiver
                usersController.delegate = self
            }
        }
    }
}


// MARK: Image Picker Controller Delegate

extension ChatController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadImageToFirebase(with: chosenImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadImageToFirebase(with image: UIImage) {
        guard let receiver = receiver else { return }
        let imageName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("channel_images").child("\(imageName).jpg")
        //Check Existing Channel
        if let channel = channel {
            if let uploadData = UIImageJPEGRepresentation(image, 0.1) {
                storageRef.putData(uploadData, metadata: nil) { metadata, error in
                    guard error == nil else { print(error!);  return }
                    storageRef.downloadURL(completion: { url, error in
                        guard error == nil else { print(error!);  return }
                        if let url = url {
                            let channelRef = Database.database().reference().child("Channel").child(channel.id)
                            let fromId = Auth.auth().currentUser!.uid
                            let timestamp = Date().timeIntervalSince1970
                            let message = Message(to: receiver.uid, from:  fromId, text: "", time: timestamp, isSeen: false,imageUrl: url.absoluteString)
                            channel.messages.append(message.toAny())
                            channelRef.updateChildValues(channel.toAny())
                        }
                    })
                }
            }
        } else {
            if let uploadData = UIImageJPEGRepresentation(image, 0.1) {
                storageRef.putData(uploadData, metadata: nil) { metadata, error in
                    guard error == nil else { print(error!);  return }
                    storageRef.downloadURL(completion: { url, error in
                        guard error == nil else { print(error!);  return }
                        if let url = url {
                            let ref = Database.database().reference().child("Channel")
                            let channelRef = ref.childByAutoId()
                            let fromId = Auth.auth().currentUser!.uid
                            let timestamp = Date().timeIntervalSince1970
                            let message = Message(to: receiver.uid, from:  fromId, text: "", time: timestamp, isSeen: false,imageUrl: url.absoluteString)
                            self.channel = Channel(id: channelRef.key, uids: [message.fromId,message.toId], messages: [message.toAny()])
                            channelRef.updateChildValues(self.channel!.toAny())
                        }
                    })
                }
            }
        }
    }
}








