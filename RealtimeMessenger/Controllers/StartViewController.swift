//
//  StartViewController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/29/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase



class StartViewController: UIViewController {
    
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfUserIsLoggedIn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    
    @IBAction func unwind(_ segue: UIStoryboardSegue) {
//        iconImageView.image = #imageLiteral(resourceName: "user profile")
//        usernameTextField.text = nil
//        passwordTextField.text = nil
//        emailTextField.text = nil
    }
    
    func checkIfUserIsLoggedIn() {
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                let uid = user!.uid
                Database.database().reference().child("Users").child(uid).observeSingleEvent(of: .value, with: { snapshot in
                    if let value = snapshot.value as? [String:Any] {
                        let key = snapshot.key
                        self.user = User(from: value,key)
                        DispatchQueue.main.async {
                            self.perform(#selector(self.handlePresentingAlreadyLoginController), with: nil, afterDelay: 0.6)
                        }
                    }else {
                        print("nil")
                    }
                    
                }, withCancel: nil)
            }else {
                self.perform(#selector(self.handlePresentingLoginController), with: nil, afterDelay: 0.6)
            }
        }
    }
    
    @objc func handlePresentingLoginController() {
        self.performSegue(withIdentifier: "Login", sender: nil)
    }
    
    @objc func handlePresentingAlreadyLoginController() {
        self.performSegue(withIdentifier: "Already Loggin", sender: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Already Loggin" {
            if let messagesVC = segue.destination.content as? MessagesController, let user = user.self {
                messagesVC.user = user
            }else {
                print(".")
            }
        }
    }

}
