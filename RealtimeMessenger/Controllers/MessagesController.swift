//
//  MessagesTableViewController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/29/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class MessagesController: UITableViewController,AddingUserDelegate {
    
    
    var user: User?
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.addCircle(withBorderWidth: 1, .black)
        }
    }

    var selectedUser: User?
    
    var messages = [Message]()
    
    var channels = [Channel]()
    
    //Handle Logout
    @IBAction func logout(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
            //self.dismiss(animated: true, completion: nil)
            //self.perform(#selector(handleLogout), with: nil, afterDelay: 0.6)
            self.performSegue(withIdentifier: "Logout", sender: nil)
        }catch {
            print(error)
        }
    }
    
    @objc func handleLogout() {
        self.performSegue(withIdentifier: "Logout", sender: nil)
    }
    
    //Adding user delegate
    func didAdd(_ user: User) {
        selectedUser = nil
        index = nil
        if selectedUser == nil {
            selectedUser = user
            perform(#selector(handlePresentChatController), with: nil, afterDelay: 0.6)
        }
    }
    
    @objc func handlePresentChatController() {
        //selectedUser = nil
        performSegue(withIdentifier: "Sending Message", sender: nil)
    }
    
    func observeChannel() {
        channels.removeAll()
        tableView.reloadData()
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        
        let ref = Database.database().reference().child("Channel")
        
        ref.observe(.childAdded, with: { snapshot in
            if let value = snapshot.value as? [String:Any] {
                let channel = Channel(from: value)
                
                channel.uids.forEach({ id in
                    if id.contains(uid) {
                        self.channels.append(channel)
                    }
                })
                
                self.attemptReloadOfTable()
            }
        }, withCancel: nil)
        
        
    }
    
    //Handle channel changes
    func observeChanges() {
        let ref = Database.database().reference().child("Channel")
        ref.observe(.childChanged, with: { snapshot in
            if let value = snapshot.value as? [String:Any] {
                let channel = Channel(from: value)
                for i in 0..<self.channels.count {
                    if self.channels[i].id.contains(channel.id) {
                        self.channels[i] = channel
                    }
                }
                self.attemptReloadOfTable()
            }
            
        }, withCancel: nil)
    }
    
    //Handle channel deleted
    func observeRemoved() {
        let ref = Database.database().reference().child("Channel")
        ref.observe(.childRemoved, with: { snapshot in
            if let value = snapshot.value as? [String:Any] {
                let channel = Channel(from: value)
                for i in 0..<self.channels.count {
                    if self.channels[i].id.contains(channel.id) {
                        self.channels.remove(at: i)
                    }
                }
                self.attemptReloadOfTable()
            }
        }, withCancel: nil)
    }
    
    private func attemptReloadOfTable() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    
    private func displayUserInfo() {
        if let user = self.user {
            self.usernameLabel.text = user.username
            if user.image != "" {
                self.profileImageView.downloaded(from: user.image, completion: nil)
            } else {
                self.profileImageView.image = #imageLiteral(resourceName: "user profile")
            }
        }
    }
    
    var timer: Timer?
    
    @objc func handleReloadTable() {
        self.messages.sort(by: { $0.timeStamp > $1.timeStamp })
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @IBAction func handleTapGesture(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "Update Profile", sender: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        observeChannel()
        observeChanges()
        observeRemoved()
        displayUserInfo()
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    
    var index: Int?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = channels[indexPath.row]
        index = indexPath.row
        //Updating message if it is read
        if let message = channel.toMessages.last ,Auth.auth().currentUser?.uid != message.fromId {
            if !message.isSeen {
                message.isSeen = true
                channel.messages.removeLast()
                channel.messages.append(message.toAny())
                Database.database().reference().child("Channel").child(channel.id).updateChildValues(channel.toAny())
            }
        }
        //Get partner id by selecting channel at index
        if let partnerId = channel.partnerId() {
            let ref = Database.database().reference().child("Users").child(partnerId)
            ref.observeSingleEvent(of: .value) { snapshot in
                if let value = snapshot.value as? [String:Any] {
                    self.selectedUser = User(from: value, partnerId)
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "Sending Message", sender: nil)
                    }
                }
            }
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Unread Message Cell", for: indexPath) as! UserCell
        let channel = channels[indexPath.row]
        
        cell.channel = channel
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Sending Message" {
            if let chatController = segue.destination.content as? ChatController {
                let backBtn = UIBarButtonItem()
                backBtn.title = "Back"
                navigationItem.backBarButtonItem = backBtn
                if selectedUser != nil {
                    chatController.receiver = selectedUser
                    chatController.uids.append(user!.uid)
                    chatController.uids.append(selectedUser!.uid)
                    //Check if channel is selected from table
                    if index != nil {
                        chatController.channel = channels[index!]
                    }
                }
            }
        } else if segue.identifier == "Update Profile" {
            if let userProfileController = segue.destination.content as? UserProfileController {
                let backBtn = UIBarButtonItem()
                backBtn.title = "Cancel"
                navigationItem.backBarButtonItem = backBtn
                userProfileController.user = user
                userProfileController.navigationItem.title = "User Profile"
            }
        } else if segue.identifier == "New Messages" {
            if let usersController = segue.destination.content as? UsersTableController {
                usersController.delegate = self
            }
        }
    }

}

//MARK: Popover Delegate
extension MessagesController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
