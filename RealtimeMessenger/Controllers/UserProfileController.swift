//
//  UserProfileController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 9/4/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import Firebase
import FirebaseStorage

class UserProfileController: UIViewController {
    
    var user: User?
    
    var imagePicker = UIImagePickerController()

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.addCircle(withBorderWidth: 0.5, .black)
        }
    }
    
    
    @IBAction func handleUpdateProfile(_ sender: UIButton) {
        guard let email = emailTextField.text
            , let username = usernameTextField.text
            , let password = passwordTextField.text else {
            return
        }
        //Check required fields
        if email.isEmpty && username.isEmpty && password.isEmpty {
            let alertController = UIAlertController(title: "Warning!", message: "Fill all required fields", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                self.usernameTextField.layer.borderColor = UIColor.red.cgColor
                self.emailTextField.layer.borderColor = UIColor.red.cgColor
                self.passwordTextField.layer.borderColor = UIColor.red.cgColor
            }))
            self.present(alertController, animated: true, completion: nil)
        } else {
            //Start update profile
            view.addSubview(loadingView)
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            if self.user!.image != "" {
                Storage.storage().reference(forURL: self.user!.image).delete { error in
                    if error != nil {
                        print("user doesnt have any images  ",error!)
                    }else {
                        print("delete successfully!")
                    }
                }
            }
            
            let storageRef = Storage.storage().reference().child("user_profileimages").child("\(uid).jpg")
            
            
            //Convert image to data
            if let uploadData = UIImageJPEGRepresentation(profileImageView.image!, 0.1) {
                storageRef.putData(uploadData, metadata: nil) { _, error in
                    if error != nil {
                        print(error!)
                        return
                    }
                    //Get url from uploaded data
                    storageRef.downloadURL(completion: { url, error in
                        if error != nil {
                            print(error!)
                            return
                        }
                        if let url = url {
                            //Updating Email
                            Auth.auth().currentUser?.updateEmail(to: email, completion: { error in
                                if error != nil {
                                    print(error!)
                                    return
                                }
                                //Updating password
                                Auth.auth().currentUser?.updatePassword(to: password, completion: { error in
                                    if error != nil {
                                        print(error!)
                                        return
                                    }
                                    //Reauthen user
                                    let credential = EmailAuthProvider.credential(withEmail: email, password: password)
                                    Auth.auth().currentUser?.reauthenticateAndRetrieveData(with: credential, completion: { authResult, error in
                                        if error != nil && authResult == nil {
                                            print(error!)
                                            return
                                        }
                                        //Save changes user to database
                                        let user = User(uid: uid, email: email, password: password, username: username, image: url.absoluteString)
                                        Database.database().reference().child("Users").child(authResult!.user.uid).updateChildValues(user.toAny())
                                        DispatchQueue.main.async {
                                            self.loadingView.removeFromSuperview()
                                            let alert  = UIAlertController(title: "Success!", message: "You have successfully updated your profile please re-login!", preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                                try! Auth.auth().signOut()
                                                self.dismiss(animated: true, completion: nil)
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                })
                            })
                        } 
                    })
                }
            }
        }
    }
    
    
    @IBAction func handleImageTapped(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //Taking photo by camera
    func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Taking photo by gallary
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func displayUserInfo() {
        if let user = user {
            emailTextField.text = user.email
            passwordTextField.text = user.password
            usernameTextField.text = user.username
            if !user.image.isEmpty {
                profileImageView.downloaded(from: user.image, completion: nil)
                spinner.stopAnimating()
            }else {
                profileImageView.image = #imageLiteral(resourceName: "user profile")
                spinner.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.center = profileImageView.center
        view.addSubview(spinner)
        spinner.startAnimating()
        imagePicker.delegate = self
        loadingView.center = view.center
        displayUserInfo()
    }
}

//MARK: Image Picker Delegate
extension UserProfileController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageView.image = chosenImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
