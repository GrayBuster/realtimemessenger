//
//  Message.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/30/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class Message {
    var toId: String
    var fromId: String
    var text: String
    var timeStamp: TimeInterval
    var isSeen: Bool
    var imageUrl: String?
    
    var chatPartnerId: String? {
        return fromId == Auth.auth().currentUser?.uid ? toId : fromId
    }
    
    init(to: String, from: String, text: String, time: TimeInterval,isSeen: Bool,imageUrl: String? = nil) {
        toId = to
        fromId = from
        self.text = text
        timeStamp = time
        self.isSeen = isSeen
        self.imageUrl = imageUrl
    }
    
    init(from value: [String:Any]) {
        toId = value["toId"] as! String
        fromId = value["fromId"] as! String
        text = value["text"] as? String ?? ""
        timeStamp = value["timestamp"] as! Double
        isSeen = value["isSeen"] as! Bool
        imageUrl = value["imageUrl"] as? String
    }
    
    func toAny() -> [String:Any] {
        if text.isEmpty, let imageUrl = imageUrl {
            return ["imageUrl":imageUrl,"toId":toId,"fromId":fromId,"timestamp":timeStamp,"isSeen":isSeen]
        }
        return ["text":text,"toId":toId,"fromId":fromId,"timestamp":timeStamp,"isSeen":isSeen]
    }
    
    func convertTimeToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        return dateFormatter.string(from: Date(timeIntervalSince1970: timeStamp))
    }
}


