//
//  Channel.swift
//  RealtimeMessenger
//
//  Created by gray buster on 9/11/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class Channel {
    var id: String
    var uids: [String]
    var messages: [[String:Any]]
    var name: String?
    
    var toMessages: [Message] {
        var mgs = [Message]()
        self.messages.forEach { (item) in
            mgs.append(Message(from: item))
        }
        return mgs
    }
    
    
    init(id: String, uids: [String],messages: [[String:Any]]) {
        self.id = id
        self.messages = messages
        self.uids = uids
    }
    
    init(from value: [String:Any]) {
        id = value["id"] as! String
        uids = value["uids"] as! [String]
        messages = value["messages"] as! [[String:Any]]
    }
    
    func creatingChannelName() {
        if uids.count > 2 {
            
        }
    }
    
    func toAny() -> [String:Any] {
        return ["id":id,"uids":uids,"messages":messages]
    }
    
    func partnerId() -> String? {
        if uids.count > 0 {
            for id in uids {
                if !id.contains(Auth.auth().currentUser!.uid) {
                    return id
                }
            }
        }
        return nil
    }
    
    func creatingChannelName(with user: User) {
        if uids.contains(user.uid) {
            if name!.isEmpty {
                name = user.username
            }else{
                name = name! + "," + user.username
            }
            
        }
    }
}
