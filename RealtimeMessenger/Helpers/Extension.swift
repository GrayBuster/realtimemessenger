//
//  Extension.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/30/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    var content: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else if let tabcon = self as? UITabBarController {
            return tabcon.viewControllers?[0].content ?? self
        }
        return self
    }
}

extension UIView {
    func addTopBorder(height: CGFloat, color: UIColor) {
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.frame.width, height: height)
        topBorder.backgroundColor = color.cgColor
        self.layer.addSublayer(topBorder)
        
    }
    func addRightBorder(width: CGFloat, color: UIColor) {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
        borderLayer.frame = CGRect(x: self.frame.width + 1, y: 0, width: width, height: self.frame.height)
        self.layer.addSublayer(borderLayer)
    }
}

extension UIImageView {
    func addCircle(withBorderWidth width: CGFloat,_ color: UIColor) {
        self.layer.borderWidth = width
        self.layer.masksToBounds = false
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

extension UIColor {
    convenience init(r: CGFloat,g: CGFloat,b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit, completion: @escaping ((Data?,URLResponse?,Error?)->())) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {

                print(error!)
                return
            }
            completion(data,response,error)
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit, completion: (()->())?) {
        
        self.image = nil
        
        if let cachedImage = imageCache.object(forKey: NSString(string: link)) as? UIImage{
            self.image = cachedImage
            return
        }
        
        guard let url = URL(string: link) else { return }
        downloaded(from: url) { data, _, error in
            if let downloadedImage = UIImage(data: data!) {
                DispatchQueue.main.async {
                    imageCache.setObject(downloadedImage, forKey: NSString(string: link))
                    self.image = downloadedImage
                }
            }
        }
    }
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
