//
//  File.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/29/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation

class User {
    var uid: String
    var email: String
    var password: String
    var username: String
    var image: String
    
    
    init(uid: String,email: String,password: String, username: String,image: String) {
        self.uid = uid
        self.email = email
        self.username = username
        self.password = password
        self.image = image
    }
    
    init(from value: [String:Any],_ key: String?) {
        if key != nil {
            self.uid = key!
        } else {
            self.uid = value["uid"] as! String
        }
        
        self.email = value["email"] as? String ?? ""
        self.username = value["username"] as? String ?? ""
        self.password = value["password"] as? String ?? ""
        self.image = value["image"] as? String ?? ""
    }
    
    func toAny() -> [String:Any] {
        return [ "email":email,
                 "password":password,
                 "username":username,
                 "uid":uid,
                 "image":image
        ]
    }
}
